﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.XR.ARFoundation;
using System.Collections;
using System.Linq;

interface IYehNahARAdapter
{
    event Action<Vector3> OnYehLocated;
    event Action<Vector3> OnNahLocated;
    event Action<Pose> OnCameraPoseChanged;
}

public class YehNahController : MonoBehaviour
{

    public GameObject Nah;
    public GameObject Yeh;

    protected IEnumerator Start()
    {
        var manager = FindObjectOfType<ARTrackedImageManager>()
            ?? throw new ApplicationException();
        var camera = Camera.main;

        Yeh.SetActive(false);
        Nah.SetActive(false);

        void onTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
        {
            var addedOrUpdated = eventArgs.added.Concat(eventArgs.updated);

            foreach (var i in addedOrUpdated)
            {
                // Debug.Log("Added or updated ref image: " + i.referenceImage.name);

                if (i.referenceImage.name == "yeh")
                {
                    Debug.Log("Updating Yeh to: " + i.transform.position);

                    Yeh.transform.position = i.transform.position;
                    Yeh.transform.LookAt(camera.transform);

                    Yeh.SetActive(true);
                }
                else if (i.referenceImage.name == "nah")
                {
                    Debug.Log("Updating Nah to: " + i.transform.position);

                    Nah.transform.position = i.transform.position;
                    Nah.transform.LookAt(camera.transform);

                    Nah.SetActive(true);
                }

            }

        }

        manager.trackedImagesChanged += onTrackedImagesChanged;

        Debug.Log("Monitoring for tracked image changes.");

        while (true)
        {


            yield return null;
        }
    }

}
